package cml.core;

import java.util.*;

public class CList extends CExpression {
	
	private ArrayList<CExpression> list;

	public CList(CExpression[] array) {
		list = new ArrayList<CExpression>(array.length);
		for (int i = 0; i < array.length; i++){
			list.add(array[i]);
		}
	}

	public CList(ArrayList<CExpression> list) {
		this.list = list;
	}

	@Override
	public String unparse() {
		StringBuilder temp = new StringBuilder("[");
		for (int i = 0; i < list.size(); i++){
			if (i != 0){
				temp.append(" ");
			}
			temp.append(list.get(i).unparse());
		}
		temp.append("]");
		return temp.toString();
	}
	
	@Override
	public String toString(){
		return "List{Size:" + list.size() + "}";
	}
	
	@Override
	public CExpression get(int index){
		return list.get(index);
	}
	
	public int length(){
		return list.size();
	}

	@Override
	public int getCardinality() {
		return list.size();
	}

	@Override
	public C getType() {
		return C.List;
	}
	
	@Override
	public CExpression replace(String alias, CExpression expr){
		ArrayList<CExpression> newlist = new ArrayList<CExpression>(list.size());
		for (int i = 0; i < list.size(); i++){
			newlist.add(i, list.get(i).replace(alias, expr));
		}
		return new CList(newlist);
	}

}
