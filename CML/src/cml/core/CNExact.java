package cml.core;

import java.math.BigInteger;

public class CNExact extends CExpression {
	
	public static final CNExact ZERO = new CNExact(), ONE = new CNExact(1);
	BigInteger bigint;
	
	public CNExact(){
		bigint = BigInteger.ZERO;
	}
	
	public CNExact(int number){
		bigint = BigInteger.valueOf(number);
	}

	@Override
	public BigInteger getBigint() {
		return bigint;
	}

	public CNExact(String number) throws NumberFormatException {
		bigint = new BigInteger(number);
	}

	public CNExact(BigInteger bigint) {
		this.bigint = bigint;
	}

	@Override
	public String unparse() {
		return bigint.toString();
	}
	
	@Override
	public String toString(){
		return bigint.toString();
	}

	@Override
	public C getType() {
		return C.NExact;
	}

}
