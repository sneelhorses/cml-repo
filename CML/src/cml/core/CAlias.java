package cml.core;

public class CAlias extends CExpression{
	
	String alias;

	public CAlias(String alias) {
		this.alias = alias;
	}

	@Override
	public String unparse() {
		return alias;
	}
	
	@Override
	public String toString(){
		return "Alias{" + alias + "}";
	}
	
	public boolean equals(String str){
		return alias.equals(str);
	}

	@Override
	public boolean isTag(int tagIndex) {
		return alias.equals(Evaluator.TAG[tagIndex]);
	}

	@Override
	public C getType() {
		return C.Alias;
	}
	
	@Override
	public CExpression replace(String alias, CExpression expr){
		return this.alias.equals(alias) ? expr : this;
	}

}
