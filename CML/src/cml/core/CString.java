package cml.core;

public class CString extends CExpression {
	
	String string;
	String literal;

	public CString(String string) {
		this.string = string;
		StringBuilder temp = new StringBuilder("\"");
		char c;
		for (int i = 0; i < string.length(); i++){
			c = string.charAt(i);
			switch (c){
			case '\t': temp.append("\\t"); break;
			case '\b': temp.append("\\b"); break;
			case '\n': temp.append("\\n"); break;
			case '\r': temp.append("\\r"); break;
			case '\f': temp.append("\\f"); break;
			case '\'': temp.append("\\\'"); break;
			case '\"': temp.append("\\\""); break;
			case '\\': temp.append("\\\\"); break;
			default: temp.append(c); break;
			}
		}
		temp.append('\"');
		literal = temp.toString();
	}

	public CString(String string, String literal) {
		this.string = string;
		this.literal = literal;
	}

	@Override
	public String unparse() {
		return literal;
	}
	
	@Override
	public String toString(){
		return string;
	}

	@Override
	public C getType() {
		return C.String;
	}

}
