package cml.core;

import java.math.*;

public class CNApprox extends CExpression {
	
	public static final CNApprox ZERO = new CNApprox();
	BigDecimal bigdec;
	
	@Override
	public BigDecimal getBigdec() {
		return bigdec;
	}

	public CNApprox(){
		bigdec = BigDecimal.ZERO;
	}

	public CNApprox(String number) throws NumberFormatException {
		bigdec = new BigDecimal(number);
	}

	public CNApprox(BigDecimal bigdec) {
		this.bigdec = bigdec;
	}

	@Override
	public String unparse() {
		String temp  = bigdec.toString();
		if (temp.contains(".")) return temp;
		return temp + ".";
	}
	
	@Override
	public String toString(){
		return bigdec.toString();
	}

	@Override
	public C getType() {
		return C.NApprox;
	}

}
