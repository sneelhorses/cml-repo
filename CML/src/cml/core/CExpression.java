package cml.core;

import java.math.*;
import java.util.*;

public abstract class CExpression {
		
	public abstract String unparse();
	
	public abstract String toString();
	
	public int getCardinality(){
		return -1;
	}
	
	public boolean isTag(int tagIndex){
		return false;
	}
	
	public BigInteger getBigint(){
		return null;
	}
	
	public BigDecimal getBigdec(){
		return null;
	}
	
	public CExpression get(int index){
		return null;
	}
	
	public CExpression replace(String alias, CExpression expr){
		return this;
	}
	
	public abstract C getType();
	
	public static CExpression parse(String expression){
		int nestLevels = 0;
		boolean inQuotes = false, escaped = false;
		StringBuilder temp = new StringBuilder();
		StringBuilder unescapedtemp = null;
		ArrayList<CExpression> templist = new ArrayList<CExpression>();
		C current = C.Unknown;
		char c;
		for (int i = 0; i < expression.length(); i++){
			c = expression.charAt(i);
			if (current == C.Unknown){
				if (c == '['){
					current = C.List;
					nestLevels++;
				} else if (Character.isDigit(c)) {
					current = C.NApprox;
					temp.append(c);
				} else if (c == '\"') {
					current = C.String;
					unescapedtemp = new StringBuilder();
					temp.append('\"');
				} else if (c == ' ' || c == ']'){
					return Evaluator.CTAG[Evaluator.INVALIDPARSE];
				} else {
					current = C.Alias;
					temp.append(c);
				}
			} else if (current == C.String){
				if (escaped){
					escaped = false;
					temp.append(c);
					switch (c){
					case 't' : unescapedtemp.append('\t'); break;
					case 'b' : unescapedtemp.append('\b'); break;
					case 'n' : unescapedtemp.append('\n'); break;
					case 'r' : unescapedtemp.append('\r'); break;
					case 'f' : unescapedtemp.append('\f'); break;
					case '\'': unescapedtemp.append('\''); break;
					case '\"': unescapedtemp.append('\"'); break;
					case '\\': unescapedtemp.append('\\'); break;
					}
				} else if (c == '\\') {
					escaped = true;
					temp.append(c);
				} else if (c == '\"') {
					return new CString(unescapedtemp.toString(), temp.toString() + "\"");
				} else {
					temp.append(c);
					unescapedtemp.append(c);
				}
			} else if (current == C.NApprox){
				temp.append(c);
			} else if (current == C.Alias){
				if (c == ' '){
					return new CAlias(temp.toString());
				}
				temp.append(c);
			} else if (current == C.List){
				if (escaped){
					temp.append(c);
					escaped = false;
				} else if (inQuotes){
					if (c == '\"'){
						inQuotes = false;
						temp.append(c);
					} else if (c == '\\'){
						temp.append(c);
						escaped = true;
					} else {
						temp.append(c);
					}
				} else {
					if (c == '\"'){
						temp.append(c);
						inQuotes = true;
					} else if (c == '['){
						temp.append(c);
						nestLevels++;
					} else if (c == ' ' && nestLevels == 1){
						if (temp.length() != 0){
							templist.add(parse(temp.toString()));
						}
						temp.setLength(0);
					} else if (c == ']'){
						nestLevels--;
						if (nestLevels == 0){
							if (temp.length() != 0){
								templist.add(parse(temp.toString()));
							}
							return new CList(templist);
						} else {
							temp.append(c);
						}
					} else {
						temp.append(c);
					}
				}
			}
		}
		if (current == C.NApprox){
			try {
				return new CNExact(temp.toString());
			} catch (NumberFormatException e){
				try {
					return new CNApprox(temp.toString());
				} catch (NumberFormatException ex){
					return Evaluator.CTAG[Evaluator.INVALIDPARSE];
				}
			}
		}
		if (current == C.Alias){
			if (temp.charAt(0) == '-' || temp.charAt(0) == '.'){

				try {
					return new CNExact(temp.toString());
				} catch (NumberFormatException e){
					try {
						return new CNApprox(temp.toString());
					} catch (NumberFormatException ex){
						return new CAlias(temp.toString());
					}
				}
			}
			return new CAlias(temp.toString());
		}
		if (current == C.List){
			if (temp.length() != 0){
				templist.add(parse(temp.toString()));
			}
			return new CList(templist);
		}
		return Evaluator.CTAG[Evaluator.INVALIDPARSE];
	}

}
