package cml.core;

import java.math.*;
import java.util.*;

public class Evaluator {
	
	public static final int CML1 = 1;
	
	private int cmlVersion = CML1;
	private static final String version = "2016-01-05";	
	
	private Map<String, CExpression> aliasTable;
	private ArrayList<CExpressionGenerator> exprGens;
	private COut out = null;

	private MathematicalComparator mathcomp = new MathematicalComparator();
	
	public static int r = 0;
	
	public static final int
			EVAL			= r++,
			UNASSOCIATION	= r++,
			DO				= r++,
			ADD				= r++,
			SUBTRACT		= r++,
			MULTIPLY		= r++,
			DIVIDE			= r++, // TODO
			PRINT			= r++,
			PRINTLN			= r++,
			INVALIDPARSE	= r++,
			VECTOR			= r++,
			MAP				= r++,
			DONE			= r++,
			ISSTRING		= r++,
			ISNUMBER		= r++,
			ISAPPROX		= r++,
			ISEXACT			= r++,
			ISLIST			= r++,
			ISDEFINED		= r++,
			ISUNDEFINED		= r++,
			ISTAG			= r++,
			ISALIAS			= r++,
			ASSIGN			= r++,
			CANT			= r++,
			CLEAR			= r++,
			LITERALASSIGN	= r++,
			ALIASTABLE		= r++,
			DOVECTOR		= r++,
			IF				= r++,
			LITERALIZATION	= r++,
			USING			= r++,
			DOTPRODUCT		= r++, // TODO implement cross-product, first requires matrices
			LITERALLOCK		= r++,
			LITERALUNLOCK	= r++,
			GET				= r++,
			FUNCTIONCALL	= r++,
			DEFINEFUNCTION	= r++,
			EQUALS			= r++, // TODO
			NOTEQUALS		= r++, // TODO
			CONCATENATION	= r++, // TODO
			AND				= r++, // TODO
			OR				= r++, // TODO
			
			
			_UPPERBOUND		= r;
	public static final String[] TAG = new String[_UPPERBOUND];
	public static final CAlias[] CTAG = new CAlias[TAG.length];
	
	public Evaluator(int cmlVersion) {
		this.cmlVersion = cmlVersion;
		aliasTable = new HashMap<String, CExpression>();
		exprGens = new ArrayList<CExpressionGenerator>();
		
		TAG[EVAL			] = "EVAL";
		TAG[UNASSOCIATION	] = "...";
		TAG[DO				] = "DO";
		TAG[ADD				] = "+";
		TAG[SUBTRACT		] = "-";
		TAG[MULTIPLY		] = "*";
		TAG[DIVIDE			] = "/";
		TAG[PRINT			] = "PRINT";
		TAG[PRINTLN			] = "PRINT-LN";
		TAG[INVALIDPARSE	] = "!E-PARSE";
		TAG[VECTOR			] = "->";
		TAG[MAP				] = "MAP";
		TAG[DONE			] = "!DONE";
		TAG[ISSTRING		] = "STRING?";
		TAG[ISNUMBER		] = "NUMBER?";
		TAG[ISAPPROX		] = "APPROX?";
		TAG[ISEXACT			] = "EXACT?";
		TAG[ISLIST			] = "LIST?";
		TAG[ISDEFINED		] = "DEFINED?";
		TAG[ISUNDEFINED		] = "UNDEFINED?";
		TAG[ISTAG			] = "TAG?";
		TAG[ISALIAS			] = "ALIAS?";
		TAG[ASSIGN			] = ";";
		TAG[CANT			] = "!E-CANT";
		TAG[CLEAR			] = "CLEAR";
		TAG[LITERALASSIGN	] = ":";
		TAG[ALIASTABLE		] = "?ALIAS-TABLE";
		TAG[DOVECTOR		] = ">>";
		TAG[IF				] = "IF";
		TAG[LITERALIZATION	] = "'";
		TAG[USING			] = "/.";
		TAG[DOTPRODUCT		] = ".P";
		TAG[LITERALLOCK		] = "`";
		TAG[LITERALUNLOCK	] = "~";
		TAG[GET				] = "GET";
		TAG[FUNCTIONCALL	] = "FUNCTION-CALL";
		TAG[DEFINEFUNCTION	] = "DEFN";
		TAG[EQUALS			] = "=";
		TAG[NOTEQUALS		] = "NEQ";
		TAG[CONCATENATION	] = "STR";
		TAG[AND				] = "AND";
		TAG[OR				] = "OR";
		
		
		for (int i = 0; i < CTAG.length; i++){
			CTAG[i] = new CAlias(TAG[i]);
		}
	}
	
	public CExpression evaluate(CExpression expr){
		if (cmlVersion == CML1){ // Check to see if CML1 rules should apply
			if (expr.getType() == C.List){ // Check if expr is a list and might need evaluation
				if (expr.getCardinality() == 0){
					return expr;
				}
				
				/*
				 * Check the list for any elements that must be evaluated due to an [EVAL expr] or [... expr]
				 */
				ArrayList<CExpression> list = new ArrayList<CExpression>();
				for (int i = 0; i < expr.getCardinality(); i++){
					CExpression n = expr.get(i);
					if (i == 0) n = evaluate(n);
					if (n.getCardinality() == 2 && !expr.get(0).isTag(LITERALASSIGN)){
						if (n.get(0).isTag(EVAL)){ // Implementation of EVAL
							list.add(evaluate(n.get(1)));
						} else if (n.get(0).isTag(UNASSOCIATION)) { // Implementation of ... (unassociation)
							CExpression temp = evaluate(n.get(1));
							if (temp.getType() == C.List){
								for (int j = 0; j < temp.getCardinality(); j++){
									list.add(temp.get(j));
								}
							} else {
								list.add(n);
							}
						} else {
							list.add(n);
						}
					} else {
						list.add(n);
					}
				}

				if (list.size() > 0 && list.get(0).getType() == C.Alias && list.size() > 1){ // Attempt to interpret it as a function
					CExpression fn = list.get(0);

					// Implementation of DO
					if (fn.isTag(DO)){
						CExpression temp = null;
						for (int i = 1; i < list.size(); i++){ // Evaluate all elements
							temp = evaluate(list.get(i));
						}
						return temp;
					}
					
					// Implementation of EVAL
					else if (fn.isTag(EVAL) && list.size() == 2){
						return evaluate(list.get(1));
						// TODO flag something
					}
					
					// Implementation of DEFINEFUNCTION
					else if (fn.isTag(DEFINEFUNCTION) && list.size() >= 4 && list.get(1).getType() == C.Alias && list.get(2).getType() == C.List){
						CExpression value = list.get(3);
						if (list.size() > 4){
							ArrayList<CExpression> temp = new ArrayList<CExpression>(list.size() - 3 + 1); // plus one for the DO
							temp.add(CTAG[DO]);
							for (int i = 3; i < list.size(); i++){
								temp.add(list.get(i));
							}
							value = new CList(temp);
						}
						// TODO scan for functions with same name
						ArrayList<CExpression> cases = new ArrayList<CExpression>();
						
						if (aliasTable.containsKey(list.get(1).unparse())){
							CExpression already = aliasTable.get(list.get(1).unparse());
							if (already.getCardinality() == 2 && already.get(0).isTag(UNASSOCIATION) && already.get(1).getCardinality() == 2 && already.get(1).get(0).isTag(LITERALIZATION) && already.get(1).get(1).getCardinality() == 2 && already.get(1).get(1).get(0).isTag(FUNCTIONCALL) && already.get(1).get(1).get(1).getType() == C.List){
								CExpression existing = already.get(1).get(1).get(1);
								for (int i = 0; i < existing.getCardinality(); i++){
									if (existing.get(i).getCardinality() - 1 != list.get(2).getCardinality()){
										cases.add(existing.get(i));
									}
								}
							}
						}
						ArrayList<CExpression> newCase = new ArrayList<CExpression>();
						newCase.add(value);
						for (int i = 0; i < list.get(2).getCardinality(); i++){
							newCase.add(list.get(2).get(i));
						}
						cases.add(new CList(newCase));
						StringBuilder builder = new StringBuilder("[... [' [FUNCTION-CALL [");
						for (int i = 0; i < cases.size(); i++){
							builder.append(cases.get(i).unparse());
							builder.append(' ');
						}
						builder.append("]]]]");
						CExpression goodnessThatTookAWhile = CExpression.parse(builder.toString());
						aliasTable.put(list.get(1).unparse(), goodnessThatTookAWhile);
						return goodnessThatTookAWhile;
					}
					
					// Implementation of FUNCTION-CALL
					// [FUNCTION-CALL [[value arg1 arg2]] (args...)]
					else if (fn.isTag(FUNCTIONCALL) && list.size() >= 2 && list.get(1).getCardinality() >= 0){
						Map<String, CExpression> replacements = new HashMap<String, CExpression>(list.size() - 2);
						int appropriateIndex = -1;
						CExpression temp = null;
						for (int i = 0; i < list.get(1).getCardinality(); i++){
							if (list.get(1).get(i).getCardinality() - 1 == list.size() - 2){
								appropriateIndex = i;
								temp = list.get(1).get(i).get(0);
								break;
							}
						}
						if (appropriateIndex == -1) return new CList(list);
						
						// this loop is in reverse so that the first argument with a name gets priority
						for (int i = list.get(1).get(appropriateIndex).getCardinality() - 1; i >= 1; i--){
							if (list.get(1).get(appropriateIndex).get(i).getType() == C.Alias) replacements.put(list.get(1).get(appropriateIndex).get(i).unparse(), list.get(i + 1));
						}
						
						for (String alias : replacements.keySet()){
							temp = temp.replace(alias, replacements.get(alias));
						}
						return evaluate(temp);
					}
					
					// Implementation of USING
					else if (fn.isTag(USING) ){
						if (list.size() > 2){
							CExpression temp  = list.get(1);
							Map<String, CExpression> replacements = new HashMap<String, CExpression>(list.size() - 2);
							for (int i = 2; i < list.size(); i++){
								if (list.get(i).getCardinality() == 2 && list.get(i).get(0).getType() == C.Alias){
									CExpression tempReplacer = list.get(i).get(1);
									for (String alias : replacements.keySet()){
										tempReplacer = tempReplacer.replace(alias, replacements.get(alias));
									}
									replacements.put(list.get(i).get(0).unparse(), tempReplacer);
								}
							}
							for (String alias : replacements.keySet()){
								temp = temp.replace(alias, replacements.get(alias));
							}
							return evaluate(temp);
							
							// EXTODO determine whether we need to do a second replace
							// Answer: no we don't
							
						} else {
							return evaluate(list.get(1));
						}
					}
					
					// Implementation of ADD
					// TODO support grouping of like terms and associative stuff
					else if (fn.isTag(ADD)){
						if (list.size() > 2){
							BigInteger bigint = BigInteger.ZERO;
							BigDecimal bigdec = BigDecimal.ZERO;
							ArrayList<CExpression> templist = new ArrayList<CExpression>();
							for (int i = 1; i < list.size(); i++){ // Evaluate all elements and combine ints and decs
								CExpression summand = evaluate(list.get(i));
								if (summand.getType() == C.NExact){
									bigint = bigint.add(summand.getBigint());
								} else if (summand.getType() == C.NApprox){
									bigdec = bigdec.add(summand.getBigdec());
								} else if (summand.getCardinality() > 0){
									if (summand.get(0).isTag(ADD)){
										for (int j = 1; j < summand.getCardinality(); j++){ // for each element in the summand (not including operator)
											list.add(summand.get(j)); // add to the list we are already scanning. this allows recursion, etc.
										}
									} else if (summand.get(0).isTag(VECTOR)){
										// TODO something else like implement it lol
										templist.add(summand);
									} else {
										templist.add(summand);
									}
								} else {
									templist.add(summand);
								}
							}
							Collections.sort(templist, mathcomp);
							templist.add(0, fn);
							if (!bigdec.equals(BigDecimal.ZERO)){
								templist.add(1, new CNApprox(bigdec.add(new BigDecimal(bigint))));
							} else if (!bigint.equals(BigInteger.ZERO)) {
								templist.add(1, new CNExact(bigint));
							}
							list = templist;
							if (list.size() == 2){
								return list.get(1); // NOTE: This cannot be condensed with the one below, otherwise it is evaluated too many times
							}
						} else if (list.size() == 2){
							return evaluate(list.get(1));
						}
						if (list.size() == 1){
							return CNExact.ZERO;
						}
					}
					
					// Implementation of MULTIPLY
					// TODO support grouping of like terms and associative stuff
					else if (fn.isTag(MULTIPLY)){
						if (list.size() > 2){
							boolean approx = false;
							BigInteger bigint = BigInteger.ONE;
							BigDecimal bigdec = BigDecimal.ONE;
							ArrayList<CExpression> templist = new ArrayList<CExpression>();
							
							
							for (int i = 1; i < list.size(); i++){ // Evaluate all elements and combine ints and decs
								CExpression multiplicand = evaluate(list.get(i));
								if (multiplicand.getType() == C.NExact){
									bigint = bigint.multiply(multiplicand.getBigint());
								} else if (multiplicand.getType() == C.NApprox){
									bigdec = bigdec.multiply(multiplicand.getBigdec());
									approx = true;
								} else if (multiplicand.getCardinality() > 0){
									if (multiplicand.get(0).isTag(MULTIPLY)){
										for (int j = 1; j < multiplicand.getCardinality(); j++){ // for each element in the multiplicand (not including operator)
											list.add(multiplicand.get(j)); // add to the list we are already scanning. this allows recursion, etc.
										}
										// TODO maybe vectors
									} else {
										templist.add(multiplicand);
									}
								} else {
									templist.add(multiplicand);
								}
							}
							Collections.sort(templist, mathcomp);
							templist.add(0, fn);
							if (!bigdec.equals(BigDecimal.ONE)){
								templist.add(1, new CNApprox(bigdec.multiply(new BigDecimal(bigint))));
							} else if (!bigint.equals(BigInteger.ONE)) {
								templist.add(1, new CNExact(bigint));
							}
							list = templist;
							if (list.size() == 2){
								return list.get(1); // NOTE: This cannot be condensed with the one below, otherwise it is evaluated too many times
							}
						} else if (list.size() == 2){
							return evaluate(list.get(1));
						}
					}
					
					// Implementation of EQUALS
					// Implementation of NOTEQUALS
					else if ((fn.isTag(EQUALS) || fn.isTag(NOTEQUALS)) && list.size() == 3){
						// TODO
					}
					
					// Implementation of SUBTRACT
					// TODO multiplication support and OPTIMIZE by fixing the parsing stuff
					else if (fn.isTag(SUBTRACT)){
						if (list.size() == 2){
							return evaluate(CExpression.parse("[" + TAG[MULTIPLY] + " -1 " + list.get(1).unparse() + "]")); // FIXME
						} else if (list.size() == 3){
							return evaluate(CExpression.parse("[+ " + list.get(1).unparse() + " [" + TAG[MULTIPLY] + " -1 " + list.get(2).unparse() + "]]")); // FIXME
						}
					}
					
					// Implementation of IF
					else if (fn.isTag(IF)){
						if (list.size() == 4){
							CExpression cond = evaluate(list.get(1));
							if (cond.getType() == C.NExact){
								if (cond.getBigint().equals(BigInteger.ZERO)){
									return evaluate(list.get(3));
								}
								return evaluate(list.get(2));
							}
							list.set(1, cond);
						}
					}
					
					// Implementation of ASSIGN
					else if (fn.isTag(ASSIGN) && list.get(1).getType() == C.Alias && list.size() == 3){
						for (int i = 0; i < TAG.length; i++){
							if (list.get(1).isTag(i)) return CTAG[CANT];
						}
						for (int i = 0; i < exprGens.size(); i++){
							if (exprGens.get(i).contains(list.get(1).unparse())) return CTAG[CANT];
						}
						CExpression temp = evaluate(list.get(2));
						aliasTable.put(list.get(1).unparse(), temp);
						return temp;
					}
					
					// Implementation of LITERALASSIGN
					else if (fn.isTag(LITERALASSIGN) && list.get(1).getType() == C.Alias && list.size() == 3){
						for (int i = 0; i < TAG.length; i++){
							if (list.get(1).isTag(i)) return CTAG[CANT];
						}
						for (int i = 0; i < exprGens.size(); i++){
							if (exprGens.get(i).contains(list.get(1).unparse())) return CTAG[CANT];
						}
						aliasTable.put(list.get(1).unparse(), list.get(2));
						return list.get(2);
					}
					
					// Implementation of DOTPRODUCT
					else if (fn.isTag(DOTPRODUCT) && list.size() > 2){
						boolean weAllGood = true;
						int size = list.get(1).getCardinality();
						for (int i = 1; i < list.size(); i++){
							if (list.get(i).getCardinality() != size || list.get(i).getCardinality() < 2 || !list.get(i).get(0).isTag(VECTOR)){
								weAllGood = false;
								break;
							}
						}
						if (weAllGood){
							ArrayList<CExpression> temp = new ArrayList<CExpression>();
							temp.add(CTAG[ADD]);
							for (int i = 1; i < size; i++){
								ArrayList<CExpression> tempMult = new ArrayList<CExpression>();
								tempMult.add(CTAG[MULTIPLY]);
								for (int j = 1; j < list.size(); j++){
									tempMult.add(list.get(j).get(i));
								}
								temp.add(new CList(tempMult));
							}
							
							return evaluate(new CList(temp));
						}
					}
					
//					// Implementation of SET
//					else if (fn.isTag(SET) && list.size() > 1){
//						ArrayList<CExpression> tempset = new ArrayList<CExpression>();
//						for (int i = 1; i < list.size(); i++){
//							tempset.add(evaluate(list.get(i)));
//						}
//						Collections.sort(tempset, mathcomp);
//						for (int i = 0; i < tempset.size() - 1; i++){
//							if (mathcomp.compare(tempset.get(i), tempset.get(i + 1)) == 0){
//								tempset.remove(i + 1);
//								
//								/*
//								 * Honestly, it's going to take more time to explain this choice than to avoid it...
//								 * Here, I decrement i to force the for loop to recheck for duplicates at index i.
//								 * The reason we want a for loop instead of a while loop is that the while loop might run past the bounds of tempset.
//								 */
//								i--;
//							}
//						}
//						tempset.add(0, CTAG[SET]);
//						return new CList(tempset);
//					}
//					
//					// Implementation of UNION
//					else if (fn.isTag(UNION) && list.size() > 1){
//						boolean weAllGood = true;
//						for (int i = 1; i < list.size(); i++){
//							list.set(i, evaluate(list.get(i)));
//							if (list.get(i).getCardinality() < 1 || !list.get(i).get(0).isTag(SET)){
//								weAllGood = false;
//								break;
//							}
//						}
//						if (weAllGood){
//							ArrayList<CExpression> tempset = new ArrayList<CExpression>();
//							for (int i = 1; i < list.size(); i++){
//								if (list.get(i).getCardinality() > 1){
//									for (int j = 1; j < list.get(i).getCardinality(); j++){
//										tempset.add(evaluate(list.get(i).get(j)));
//									}
//								}
//							}
//							Collections.sort(tempset, mathcomp);
//							for (int i = 0; i < tempset.size() - 1; i++){
//								if (mathcomp.compare(tempset.get(i), tempset.get(i + 1)) == 0){
//									tempset.remove(i + 1);
//									
//									/*
//									 * Honestly, it's going to take more time to explain this choice than to avoid it...
//									 * Here, I decrement i to force the for loop to recheck for duplicates at index i.
//									 * The reason we want a for loop instead of a while loop is that the while loop might run past the bounds of tempset.
//									 */
//									i--;
//								}
//							}
//							tempset.add(0, CTAG[SET]);
//							return new CList(tempset);
//						}
//					}
					
					// Implementation of GET
					else if (fn.isTag(GET) && list.size() == 3 && list.get(2).getType() == C.NExact){
						CExpression temp = evaluate(list.get(1));
						if (list.get(2).getBigint().compareTo(BigInteger.valueOf(temp.getCardinality())) < 0 && list.get(2).getBigint().compareTo(BigInteger.ZERO) >= 0){
							return temp.get(list.get(2).getBigint().intValue());
						}
					}
					
					// Implementation of MAP
					else if (fn.isTag(MAP)){
						if (list.size() > 2){
							ArrayList<CExpression> bigtemp = new ArrayList<CExpression>();
							// temp.get(0); returns a list
							boolean going = true;
							int r = 0;
							while (going){
								going = false;
								ArrayList<CExpression> smalltemp = new ArrayList<CExpression>();
								for (int i = 1; i < list.size(); i++){
									if (list.get(i).getCardinality() > r){
										going = true;
										smalltemp.add(list.get(i).get(r));
									} else if (list.get(i).getType() != C.List) {
										smalltemp.add(list.get(i));
									}
								}
								if (going) bigtemp.add(evaluate(new CList(smalltemp)));
								r++;
							}
							list = bigtemp;
						}
					}
					
					// Implementation of CLEAR
					else if (fn.isTag(CLEAR)){
						for (int i = 0; i < TAG.length; i++){
							if (list.get(1).isTag(i)) return CTAG[CANT];
						}
						for (int i = 0; i < exprGens.size(); i++){
							if (exprGens.get(i).contains(list.get(1).unparse())) return CTAG[CANT];
						}
						aliasTable.remove(list.get(1).unparse());
						return CTAG[DONE];
					}

					// Implementation of PRINT
					else if (fn.isTag(PRINT) && out != null){
						for (int i = 1; i < list.size(); i++){
							out.output(evaluate(list.get(i)).toString());
						}
						return CTAG[DONE];
					}
					
					// Implementation of PRINTLN
					else if (fn.isTag(PRINTLN) && out != null){
						for (int i = 1; i < list.size(); i++){
							out.output(evaluate(list.get(i)).toString());
							out.output("\n");
						}
						return CTAG[DONE];
					}
					
					// Implementation of VECTOR
					else if (fn.isTag(VECTOR)){
						for (int i = 1; i < list.size(); i++){
							list.set(i, evaluate(list.get(i)));
						}
					}
					
					// Implementation of DOVECTOR
					else if (fn.isTag(DOVECTOR)){
						for (int i = 1; i < list.size(); i++){
							list.set(i, evaluate(list.get(i)));
						}
						list.remove(0);
					}
					
					// Implementation of ISSTRING
					else if (fn.isTag(ISSTRING)){
						return list.get(1).getType() == C.String ? CNExact.ONE : CNExact.ZERO;
					}
					
					// Implementation of ISNUMBER
					else if (fn.isTag(ISNUMBER)){
						return list.get(1).getType() == C.NApprox || list.get(1).getType() == C.NExact ? CNExact.ONE : CNExact.ZERO;
					}
					
					// Implementation of ISAPPROX
					else if (fn.isTag(ISAPPROX)){
						return list.get(1).getType() == C.NApprox ? CNExact.ONE : CNExact.ZERO;
					}
					
					// Implementation of ISEXACT
					else if (fn.isTag(ISEXACT)){
						return list.get(1).getType() == C.NExact ? CNExact.ONE : CNExact.ZERO;
					}
					
					// Implementation of ISLIST
					else if (fn.isTag(ISLIST)){
						return list.get(1).getType() == C.List ? CNExact.ONE : CNExact.ZERO;
					}
					
					// Implementation of ISDEFINED
					else if (fn.isTag(ISDEFINED)){
						for (int i = 0; i < TAG.length; i++){
							if (list.get(1).isTag(i)) return CNExact.ONE;
						}
						for (int i = 0; i < exprGens.size(); i++){
							if (exprGens.get(i).contains(list.get(1).unparse())) return CNExact.ONE;
						}
						if (aliasTable.containsKey(list.get(1).unparse())) return CNExact.ONE;
						return CNExact.ZERO;
					}
					
					// Implementation of ISUNDEFINED
					else if (fn.isTag(ISUNDEFINED)){
						for (int i = 0; i < TAG.length; i++){
							if (list.get(1).isTag(i)) return CNExact.ZERO;
						}
						for (int i = 0; i < exprGens.size(); i++){
							if (exprGens.get(i).contains(list.get(1).unparse())) return CNExact.ZERO;
						}
						if (aliasTable.containsKey(list.get(1).unparse())) return CNExact.ZERO;
						return CNExact.ONE;
					}
					
					// Implementation of ISTAG
					else if (fn.isTag(ISTAG)){
						for (int i = 0; i < TAG.length; i++){
							if (list.get(1).isTag(i)) return CNExact.ONE;
						}
						return CNExact.ZERO;
					}
					
					// Implementation of ISALIAS
					else if (fn.isTag(ISALIAS)){
						return list.get(1).getType() == C.Alias ? CNExact.ONE : CNExact.ZERO;
					}
					
					// Implementation of LITERALIZATION
					else if (fn.isTag(LITERALIZATION) && list.size() == 2){
						return list.get(1);
					}
					
					// Implementation of LITERALLOCK doesn't really exist
					// Implementation of LITERALUNLOCK
					else if (fn.isTag(LITERALUNLOCK) && list.size() == 2){
						if (list.get(1).getCardinality() == 2 && list.get(1).get(0).equals(CTAG[LITERALUNLOCK])){
							return evaluate(list.get(1).get(1));
						}
					}
					
				}
				return new CList(list);
				
			} else if (expr.getType() == C.Alias) {
				String key = expr.unparse();
				
				// Implementation of ALIASTABLE
				if (expr.isTag(ALIASTABLE)){
					ArrayList<CExpression> aliases = new ArrayList<CExpression>(aliasTable.size());
					for (String k : aliasTable.keySet()){
						aliases.add(new CList(new CExpression[]{new CAlias(k), aliasTable.get(k)}));
					}
					return new CList(aliases);
				}
				
				for (int i = 0; i < exprGens.size(); i++){
					if (exprGens.get(i).contains(key)) return exprGens.get(i).get(key); // TODO maybe this should be evaluted
				}
				if (aliasTable.containsKey(key)) return evaluate(aliasTable.get(key));
				return expr; // TODO might be redundant
			}
		}
		return expr;
	}

	public int getCMLVersion() {
		return cmlVersion;
	}

	public void setCMLVersion(int cmlVersion) {
		this.cmlVersion = cmlVersion;
	}
	
	public void putAlias(String alias, CExpression expr){
		aliasTable.put(alias, expr);
	}
	
	public void setOut(COut out) {
		this.out = out;
	}
	
	public void addCExpressionGenerator(CExpressionGenerator gen){
		exprGens.add(gen);
	}
	
	public void removeCExpressionGenerator(CExpressionGenerator gen){
		exprGens.remove(gen);
	}
	
	public static String getVersion(){
		return version;
	}

}
