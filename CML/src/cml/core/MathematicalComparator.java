package cml.core;

import java.util.*;

public class MathematicalComparator implements Comparator<CExpression> {

	@Override
	public int compare(CExpression a, CExpression b) {
		int sa = 100;
		int sb = 100;
		
		if (a.getType() == C.NApprox) 	sa = 0;
		if (a.getType() == C.NExact)	sa = 1;
		if (a.getType() == C.Alias)		sa = 2;
		if (a.getType() == C.List)		sa = 4;
		if (a.getCardinality() == 0)	sa = 3;
		if (a.getType() == C.String)	sa = 5;
		
		if (b.getType() == C.NApprox) 	sb = 0;
		if (b.getType() == C.NExact)	sb = 1;
		if (b.getType() == C.Alias)		sb = 2;
		if (b.getType() == C.List)		sb = 4;
		if (b.getCardinality() == 0)	sb = 3;
		if (b.getType() == C.String)	sb = 5;
		
		if (sa - sb != 0) return sa - sb;
		
		if (a.getType() == C.NApprox) return (a.getBigdec().compareTo(b.getBigdec()));
		if (a.getType() == C.NExact) return (a.getBigint().compareTo(b.getBigint()));
		if (a.getType() == C.Alias) return (a.unparse().compareTo(b.unparse()));
		if (a.getType() == C.String) return (a.unparse().compareTo(b.unparse()));
		if (a.getCardinality() == 0) return 0;
		if (a.getType() == C.List) {
			for (int i = 0; i < a.getCardinality(); i++){
				if (i >= b.getCardinality()) return 1;
				int compVal = compare(a.get(i), b.get(i));
				if (compVal != 0) return compVal;
			}
			return a.getCardinality() == b.getCardinality() ? 0 : -1;
		}
		return 0; // If both are unknown types of CExpression
	}

}
