package cml.core;

public interface CExpressionGenerator {
	public boolean contains(String key);
	public CExpression get(String key);
}
