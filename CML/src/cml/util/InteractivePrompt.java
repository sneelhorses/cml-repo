package cml.util;

import cml.core.*;
import java.util.*;

public class InteractivePrompt {

	public static void main(String[] args){
		System.out.println("\n"
				+ "CML Interactive Prompt using the " + Evaluator.getVersion() + " version of CML.\n"
				+ "Designed and developed by Brandon Canfield.\n");
		Scanner in = new Scanner(System.in);
		Evaluator evaluator = new Evaluator(Evaluator.CML1);
		//evaluator.putConst("?ABOUT", new CString("CML Interactive Prompt"));
		CExpression expr;
		boolean done = false;
		String str;
		do {
			System.out.print("CML: ");
			str = in.nextLine();
			expr = CExpression.parse(str);
			System.out.println("   > " + expr.getType());
			System.out.println("  -> " + expr.unparse());
			expr = evaluator.evaluate(expr);
			System.out.println("  => " + expr.unparse());
			if (expr.getType() == C.String)	System.out.println(" ==> " + expr.toString());
			System.out.println();
		} while (!str.equals("EXIT"));
	}

}
